# salut

A notification daemon for Wayland compositors made with [snui](https://gitlab.com/snakedye/snui).

![](/images/animation.webm)

[![Packaging status](https://repology.org/badge/vertical-allrepos/salut.svg)](https://repology.org/project/salut/versions)

## Features
- Summary
- Body
- Icons (SVGs and PNGs)
- Timeout
- Actions
- Urgency
- Persistence
- Animations
- Light, dark and custom themes

## Documentation

For information about how to use, install or configure salut, visit the [Wiki](https://gitlab.com/snakedye/salut/-/wikis/Home).


